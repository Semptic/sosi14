# README #

Projektarbeit für Software Sicherheit 2014

### Usermanagment ###

Die User werden über devise verwaltet. Um einen Controller von devise verwalten zu lassen muss ein

    before_action :authenticate_user!

an den anfang des Controllers hinzugefügt werden.

Es existieren folgende Rollen:

* registered
* student
* professor
* admin

Diese können durch `current_user.is_<ROLE>` abgefragt werden. Die funktionen jeweils sind:

* `is_registered`
* `is_professor`
* `is_student`
* `is_admin`

Zudem wurden folgende Benutzer angelegt:

    User.create(email: 'admin@test.de',       password: 'admin123',       role: Role.find_by(name: 'admin'))
    User.create(email: 'student@test.de',     password: 'student123',     role: Role.find_by(name: 'student'))
    User.create(email: 'registered@test.de',  password: 'registered123',  role: Role.find_by(name: 'register'))
    User.create(email: 'professor@test.de',   password: 'professor123',   role: Role.find_by(name: 'professor'))