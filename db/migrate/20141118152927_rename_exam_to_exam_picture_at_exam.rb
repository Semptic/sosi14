class RenameExamToExamPictureAtExam < ActiveRecord::Migration
  def change
    rename_column :exams, :exam, :exam_picture
  end
end
