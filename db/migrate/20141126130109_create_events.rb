class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :text
      t.datetime :start
      t.string :host
      t.string :location

      t.timestamps
    end
  end
end
