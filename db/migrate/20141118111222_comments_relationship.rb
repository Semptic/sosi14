class CommentsRelationship < ActiveRecord::Migration
  def change
    add_column :comments, :exam_id, :integer
    add_index :comments, :exam_id
  end
end
