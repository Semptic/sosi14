class ExamRelationships < ActiveRecord::Migration
  def change
    add_column :exams, :professor_id, :integer
    add_index :exams, :professor_id
    add_column :exams, :student_id, :integer
    add_index :exams, :student_id
  end
end
