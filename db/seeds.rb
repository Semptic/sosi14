# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
['registered', 'student', 'professor', 'admin'].each do |role|
  Role.find_or_create_by({name: role})
end

User.create(email: 'admin@test.de',       password: 'admin123',       role: Role.find_by(name: 'admin'))
User.create(email: 'student@test.de',     password: 'student123',     role: Role.find_by(name: 'student'))
User.create(email: 'registered@test.de',  password: 'registered123',  role: Role.find_by(name: 'register'))
User.create(email: 'professor@test.de',   password: 'professor123',   role: Role.find_by(name: 'professor'))