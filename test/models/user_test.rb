require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "User get default registered role and checks helper functions" do
    user = User.new(email: 'test@mail.de', password: "secret123")
    user.save!
    
    assert user.is_registered
    assert_not user.is_admin
    assert_not user.is_student
    assert_not user.is_professor
  end

  test "User gets new role and check helper functions" do
    user = User.new(email: 'test@mail.de', password: 'secret123')
    user.save!

    user.role = Role.find_by(name: 'admin')
    user.save!

    assert user.is_registered
    assert user.is_admin
    assert_not user.is_student
    assert_not user.is_professor
  end
end
