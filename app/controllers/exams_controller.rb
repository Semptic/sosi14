class ExamsController < ApplicationController
  before_action :set_exam, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: []
  respond_to :html

  def index

    @user = current_user
    if current_user.is_student
      @exams = Exam.where(student: current_user.id)
      render :index_student
    else
      @exams = Exam.where(professor: current_user.id)
      render :index_professor
    end
  end

  def show
    @comment = Comment.new
    respond_with(@exam)
  end

  def new
    if current_user.is_professor
      @exam = Exam.new
      @students_array = User.get_students.map { |student| [student.email, student.id] }
      respond_with(@exam)
    else
      render(json: {msg: 'not authorized'}, status: 401)
    end
  end

  def edit
    if current_user.is_professor
      @students_array = User.get_students.map { |student| [student.email, student.id] }
    else
      render(json: {msg: 'not authorized'}, status: 401)
    end
  end

  def create
    if current_user.is_professor
      requests_params = params[:exam]
      requests_params[:professor_id] = current_user.id
      @exam = Exam.new(exam_params)
      @exam.save
      respond_with(@exam)
    else
      render(json: {msg: 'not authorized'}, status: 401)
    end
  end

  def update
    if current_user.is_professor
      @exam.update(exam_params)
      respond_with(@exam)
    else
      render(json: {msg: 'not authorized'}, status: 401)
    end
  end

  def destroy
    if current_user.is_professor
      @exam.destroy
      respond_with(@exam)
    else
      render(json: {msg: 'not authorized'}, status: 401)
    end
  end

  private
  def set_exam
    @exam = Exam.find(params[:id])
  end

  def exam_params
    params.require(:exam).permit(:grade, :title, :exam_picture, :student_id, :professor_id)
  end
end
