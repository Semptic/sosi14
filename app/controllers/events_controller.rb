class EventsController < ApplicationController

  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    if current_user.is_registered
      @user = current_user
      @events = Event.all
    else
      render(json: {msg: 'not authorized'}, status: 401)
    end
  end

  def new
    if current_user.is_professor
      @event = Event.new
    else
      render(json: {msg: 'not authorized'}, status: 401)
    end
  end

  def create
    if current_user.is_professor
      @event = Event.new(event_params)
      @event.save
      redirect_to @event
    else
      render(json: {msg: 'not authorized'}, status: 401)
    end
  end

  def edit
    if current_user.is_professor
      @event = Event.find(params[:id])
    else
      render(json: {msg: 'not authorized'}, status: 401)
    end

  end

  def update
    if current_user.is_professor
      @event.update(event_params)
      redirect_to @event
    else
      render(json: {msg: 'not authorized'}, status: 401)
    end
  end


  def show
    @event = Event.find(params[:id])
  end

   def destroy
    if current_user.is_professor
      @event.destroy
      redirect_to events_path
    else
      render(json: {msg: 'not authorized'}, status: 401)
    end
  end

  private
  def set_event
    @event = Event.find(params[:id])
  end

  def event_params
    params.require(:event).permit(:title, :text, :start, :host, :location)
  end
end
