class UserController < ApplicationController
  def edit
    if current_user.is_admin
      role_id = params[:post][:role]
      user_id = params[:user_id]

      user = User.find(user_id).update(role: Role.find(role_id))
    end
    redirect_to '/admin'
  end
end
