class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  belongs_to :role
  validates_associated :role
  before_create :set_default_role
  has_many :exams

  def self.get_students
    User.joins(:role).where(roles: {name:'student'})
  end

  def is_registered
    if self.role.name == 'registered'
      return true
    elsif self.is_professor
      return true
    elsif self.is_student
      return true
    elsif self.is_admin
      return true
    else
      return false
    end
  end

  def is_professor
    if self.role.name == 'professor'
      return true
    else
      return false
    end
  end

  def is_student
    if self.role.name == 'student'
      return true
    else
      return false
    end
  end

  def is_admin
    if self.role.name == 'admin'
      return true
    else
      return false
    end
  end

  private
  def set_default_role
    self.role ||= Role.find_by_name('registered')
  end
end
