class Exam < ActiveRecord::Base

  require 'carrierwave/orm/activerecord'
  has_many :comments
  belongs_to :student, class_name: "User", foreign_key: "student_id"
  belongs_to :professor, class_name: "User", foreign_key: "professor_id"
  mount_uploader :exam_picture, ExamPictureUploader
end
