json.array!(@exams) do |exam|
  json.extract! exam, :id, :grade
  json.url exam_url(exam, format: :json)
end
